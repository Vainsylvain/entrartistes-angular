export class Salle {
  constructor(
  public uid: string,
  public region: String,
  public tagsS: String,
  public placename: String,
  public timetable: String,
  public city: String,
  public address: String,
  public date_start: String,
  public date_end: String,
  public city_district: String,
  public description: String,
  public pricing_info: String,
  public title: String,
  public free_text: String,
  public latlon: []
  ){}
}
