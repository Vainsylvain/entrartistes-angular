import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, throwError } from 'rxjs';
import { environment } from '../../../environments/environment';

export const TOKEN = 'token';
export const AUTHENTICATEDUSER = 'authenticaterUser';

const AUTH_API = environment.url_api_spring_auth;

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class LoginService{

  constructor(private _http : HttpClient) { }

//----------------------------------------------------------------LOGIN---------------------------------------------------------------------
  login(username: string, password: string): Observable<any> {
    return this._http.post(AUTH_API + 'signin', {
      username,
      password
    }, httpOptions);
  }

}
