import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

const AUTH_API = environment.url_api_spring_auth;
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  public messageRegister :any;

  constructor(private _http : HttpClient) { }

  //----------------------------------------------------------------REGISTER---------------------------------------------------------------------
  register(username: string, email: string, password: string, roles : string[], lastName: string, firstName: string, birthday : string, numberStreet: string, streetName: string, postalCode: string, city:string, domain :string, location : string, sceneName:string,slogan:string, biographie:string, imageName:string, locations : string[], domains: string[]): Observable<any> {
    return this._http.post(AUTH_API + 'signup', {
      username,
      email,
      password,
      roles,
      lastName,
      firstName,
      birthday,
      numberStreet,
      streetName,
      postalCode,
      city,
      domain,
      location,
      sceneName,
      slogan,
      biographie,
      imageName,
      locations,
      domains
    }, httpOptions);
  }
}
