import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Department } from '../data/department';

const AUTH_API = environment.url_api_node_search;

@Injectable({
  providedIn: 'root'
})
export class SallesService {

  constructor(private _http : HttpClient) { }

//----------------------------------------------------GET_DEPARTMENTS--------------------------------------------------
  getDepartments(){
    return this._http.get(AUTH_API + 'departments');
  }

  //----------------------------------------------------GET_DEPARTMENTS--------------------------------------------------
  getTagsP(){
    return this._http.get(AUTH_API + 'tagsP');
  }

  //----------------------------------------------------GET_DEPARTMENTS--------------------------------------------------
  getTagsS(){
    return this._http.get(AUTH_API + 'tagsS');
  }

  getListSalles(department: string, tagP: string, tagsS: String[] ){
    var paramTagP ="";
    for(let i in tagsS){
      paramTagP += "&tagsS=";
      paramTagP += tagsS[i];
    }

    var paramTagS ="";
    if(tagsS.length==0){
      paramTagS="";
    }
    else if(tagsS.length==1){
      paramTagS="&tagS="+tagsS[0];
    }
    else{
      for(let i in tagsS){
        paramTagS+="&tagS="+tagsS[i];
      }
    }

    let url = AUTH_API+ `search/tagS/?department=`+department+"&tagP="+tagP+paramTagP+paramTagS;
    console.log(url);
      return this._http.get(url);
  }
}
