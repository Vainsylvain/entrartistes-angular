import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

const API_URL = environment.url_api_spring_artist;

@Injectable({
  providedIn: 'root'
})
export class ProfilService {

  constructor(private http: HttpClient) { }

  getArtistBoard(): Observable<any> {
    return this.http.get(API_URL + 'userconnect', { responseType: 'text' });
  }

  getArtistPrefs(username : string):Observable<any> {
    return this.http.get(API_URL + 'artistprefs/'+username);
  }

}
