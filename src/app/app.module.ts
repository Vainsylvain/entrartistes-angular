import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './APIs/API_SPRING/login/login.component';
import { RegisterComponent } from './APIs/API_SPRING/register/register.component';
import {HttpClientModule,} from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import {MatStepperModule} from '@angular/material/stepper';
import { ProfileComponent } from './APIs/API_SPRING/profile/profile.component';
import { BoardAdminComponent } from './APIs/API_SPRING/board-admin/board-admin.component';
import { BoardArtistComponent } from './APIs/API_SPRING/board-artist/board-artist.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyAuthInterceptor } from './common/interceptors/my-auth.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule } from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatRippleModule} from '@angular/material/core';
import {MatInputModule} from '@angular/material/input';
import { DatePipe } from '@angular/common';
import { SearchSalleComponent } from './APIs/API_NODE/search-salle/search-salle.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    BoardAdminComponent,
    BoardArtistComponent,
    SearchSalleComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatStepperModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatRippleModule,
    MatInputModule
  ],
  providers:[MyAuthInterceptor, DatePipe],
  bootstrap: [AppComponent],
})
export class AppModule {}
