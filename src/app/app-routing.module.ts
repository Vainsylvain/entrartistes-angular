import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchSalleComponent } from './APIs/API_NODE/search-salle/search-salle.component';
import { BoardAdminComponent } from './APIs/API_SPRING/board-admin/board-admin.component';
import { BoardArtistComponent } from './APIs/API_SPRING/board-artist/board-artist.component';
import { LoginComponent } from './APIs/API_SPRING/login/login.component';
import { ProfileComponent } from './APIs/API_SPRING/profile/profile.component';
import { RegisterComponent } from './APIs/API_SPRING/register/register.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home'},
  { path: 'home', component: HomeComponent},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'profile', component: ProfileComponent },
  { path: 'user', component: BoardArtistComponent },
  { path: 'admin', component: BoardAdminComponent },
  { path: 'search', component: SearchSalleComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
