import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Login } from '../common/data/login';
import { LoginService } from '../common/services/login.service';
import { ProfilService } from '../common/services/profil.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  content?: string;
  isLatNav = false;

  images = [
    { img: "../../assets/img/concert-rouge.jpg", title: "Slide 1" },
    { img: "../../assets/img/defile-rouge-noir.jpg", title: "Slide 2" },
    { img: "../../assets/img/chaussons-danse.jpg", title: "Slide 3" }
  ];

  constructor(private profilService: ProfilService) { }

  ngOnInit(): void {}

}
