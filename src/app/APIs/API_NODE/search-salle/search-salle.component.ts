import { Component, OnInit } from '@angular/core';
import { Department } from 'src/app/common/data/department';
import { Salle } from 'src/app/common/data/salle';
import { Type } from 'src/app/common/data/type';
import { SallesService } from 'src/app/common/services/salles.service';

@Component({
  selector: 'app-search-salle',
  templateUrl: './search-salle.component.html',
  styleUrls: ['./search-salle.component.scss'],
})
export class SearchSalleComponent implements OnInit {
  //*********************************************VARIABLES*****************************************
  //-------------------Departments-----------------
  allDeps: string[] = [];
  depSelected: string = '';

  //-------------------Tags-----------------------
  allTagsP: string[] = [];
  tagPSelected: string = '';

  allTagsS: string[] = [];
  tagsSSelected: string[] = [];
  selectedTagS : string ="";

  //-----------------Salles-----------------------
  sallesFoundtagP: Salle[] = [];
  sallesFoundtagS: Salle[] = [];

  //-----------------Messages---------------------
  sallesPNotFound=true;
  salleSNotFound=true;
  messageFoundTagP = '';
  messageFoundTagS = '';

  //*********************************************CONSTRUCTOR_&_ONINIT*****************************************
  constructor(private salleService: SallesService) {}

  ngOnInit() {
    this.getDeps();
    this.getTagsP();
    this.getTagsS();
  }

  //*********************************************METHODES*****************************************

  //------------------------------Inits-------------------------------
  getDeps() {
    this.salleService.getDepartments().subscribe({
      next: (data: any) => {
        let depObj: Department[];
        depObj = data;
        for (let i in depObj) {
          this.allDeps.push(depObj[i].department);
        }
      },
      error: (err) => {
        console.log(err);
      },
    });
  }
  reinit(){
    this.allDeps = [];
    this.depSelected = '';
    this.allTagsP = [];
    this.tagPSelected = '';
    this.allTagsS = [];
    this.tagsSSelected = [];
    this.sallesFoundtagP = [];
    this.sallesFoundtagS = [];
    this.sallesPNotFound=true;
    this.salleSNotFound=true;
    this.messageFoundTagP = '';
    this.messageFoundTagS = '';
    this.getDeps();
      this.getTagsP();
      this.getTagsS();
    }

  //------------------------------Selects-------------------------------
  selectDep(event: any) {
    if (event.target.value == 'Sélectionner département') {
      this.depSelected = '';
    } else {
      this.depSelected = event.target.value;
    }
  }

  selectTagP(event: any) {
    if (
      event.target.value == 'Sélectionner Type de spectacle' ||
      event.target.value == 'Cirque'
    ) {
      this.tagPSelected = '';
      this.allTagsS = [];
    }
    else {
      this.tagPSelected = event.target.value;
    }
    this.getTagsS();
  }

  selectTagsS(event: any) {
    if (event.target.value == 'Sélectionner Types secondaires') {
      this.selectedTagS = '';
      this.allTagsS = [];
    } else {
      this.selectedTagS = event.target.value;
      this.tagsSSelected.push(this.selectedTagS);
    }
  }
    deleteTagS(dom: number) {
      this.tagsSSelected.splice(dom, 1);
    }


  //------------------------------Filtre_Tags--------------------------
  getTagsS() {
    this.salleService.getTagsS().subscribe({
      next: (data: any) => {
        let tagSobj: Type[];
        tagSobj = data;
        for (let i in tagSobj) {
          if (tagSobj[i].tagP == this.tagPSelected) {
            this.allTagsS = tagSobj[i].tagsS;
          }
        }
      },
      error: (err) => {
        console.log(err);
      },
    });
  }

  getTagsP() {
    this.salleService.getTagsP().subscribe({
      next: (data: any) => {
        let tagPObj: string[];
        tagPObj = data;
        for (let i in tagPObj) {
          this.allTagsP.push(tagPObj[i]);
        }
      },
      error: (err) => {
        console.log(err);
      },
    });
  }

  //------------------------------Search_Salles--------------------------

  goGetSalles() {
    console.log("leeeength"+this.tagsSSelected.length);
    this.salleService
      .getListSalles(this.depSelected, this.tagPSelected, this.tagsSSelected)
      .subscribe({
        next: (data: any) => {
          try {
            this.sallesFoundtagP = data.listP;
            this.sallesPNotFound = false;
          } catch {
            this.messageFoundTagP =
              "Aucune salle correspondant à vos critères ne remonte chez nous, mais cela ne signifie pas qu'il n'existe pas la perle rare pour vous ;).";
          }
          try {
            this.sallesFoundtagS = data.listS;
            this.salleSNotFound = false;
          } catch {
            this.messageFoundTagS =
              "Aucune salle correspondant à vos critères n'a été trouvée. Vous pouvez peut-être essayer avec moins de critères secondaires ;).";
          }
          console.log("status sallP="+this.sallesPNotFound);
        },
        error: (err) => {
          console.log(err);
        },
      });
  }
}
