import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchSalleComponent } from './search-salle.component';

describe('SearchSalleComponent', () => {
  let component: SearchSalleComponent;
  let fixture: ComponentFixture<SearchSalleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchSalleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchSalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
