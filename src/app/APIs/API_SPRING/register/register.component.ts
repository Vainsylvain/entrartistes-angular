import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from 'src/app/common/services/register.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {

  //*********************************************VARIABLES*****************************************
  //---------------------formu_register-----------------
  form: any = {
    lastName: null,
    firstName: null,
    birthday: null,

    numberStreet: null,
    streetName: null,
    postalCode: null,
    city: null,

    username: null,
    email: null,
    password: null,

    domain: null,
    location: null,
    sceneName: null,
    slogan: null,
    biographie: null,
    imageName: null,
    locations: null,
    domains: null,
  };

  roles: string[] = [];

  //-----------------------SignUp--------------------------
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  successMessage="";

  //-----------------------Files--------------------------

  selectedFile!: File;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  message: string = '';
  imageName: any;

  //-----------------------selects--------------------------

  optionsDepartments: string[] = [];
  selectedDepartment: string = '';
  selectedDepartements: string[] = [];
  optionsDomains: string[] = [];
  selectedDomain: string = '';
  selectedDomains: string[] = [];

  //*********************************************CONSTRUCTOR_&_ONINIT*****************************************
  constructor(
    private registerService: RegisterService,
    private router: Router,
    private httpClient: HttpClient,
    public datepipe: DatePipe
  ) {}

  ngOnInit(): void {
    this.form.birthday = new Date();
    console.log(this.form.birthday);
    this.optionsDepartments = ['option 1', 'option 2', 'option 3'];
    this.optionsDomains = ['optiond 1', 'optiond 2', 'optiond 3'];
  }

  //*********************************************METHODES*****************************************
  //----------------------OnSubmit------------------------
  onSubmit(): void {
    this.form.domains = this.selectedDomains;
    this.form.locations = this.selectedDepartements;
    this.form.imageName = this.imageName;
    console.log(this.form.birthday);
    this.form.birthday = this.datepipe.transform(
      this.form.birthday,
      'yyyy-MM-dd'
    );
    console.log(this.form.birthday);
    const {
      username,
      email,
      password,
      lastName,
      firstName,
      birthday,
      numberStreet,
      streetName,
      postalCode,
      city,
      domain,
      location,
      sceneName,
      slogan,
      biographie,
      imageName,
      locations,
      domains,
    } = this.form;
    this.roles.push('artist');
    this.registerService
      .register(
        username,
        email,
        password,
        this.roles,
        lastName,
        firstName,
        birthday,
        numberStreet,
        streetName,
        postalCode,
        city,
        domain,
        location,
        sceneName,
        slogan,
        biographie,
        imageName,
        locations,
        domains
      )
      .subscribe({
        next: (data) => {
          console.log(data);
          this.isSuccessful = true;
          this.isSignUpFailed = false;
          this.roles = [];
          this.successMessage = "Vous êtes bien inscrits, vous pouvez maintenant vous connecter :)"
          setTimeout(()=>{this.router.navigate(['login'])},9000);
        },
        error: (err) => {
          console.log(err);
          this.roles = [];
          this.errorMessage = err.error;
          this.isSignUpFailed = true;
        },
      });
  }

  //-----------------------------PreRemplir-----------------------------
  preRemplir(): void {
    this.form.lastName = 'Blanco';
    this.form.firstName = 'Judith';
    this.form.birthday = '1990/02/04';
    this.form.numberStreet = '8';
    this.form.streetName = 'avenue des libellules';
    this.form.postalCode = '88000';
    this.form.city = 'Epinal';
    this.form.username = 'Judi';
    this.form.email = 'j@j.com';
    this.form.password = 'jjjjjj';
    (this.form.sceneName = 'Compagnie Judi'),
      (this.form.slogan = 'Comédienne - Auteure - Poète - Metteuse en scène'),
      (this.form.biographie =
        "Passionnée depuis l'enfance de lectures théâtrales, j'ai grandi dans le monde des dramaturge. J'ai suivi des cours depuis mes 5 ans puis j'ai présidé le club de théâtre de mon lycée. Je me suis ensuite tournée vers des études de théâtre au Conservatoire Régional du Grand Nancy. Depuis, j'ai créé deux oeuvres intitulées <<Le Coeur Vibrant>> et <<Les Bassines en Or>> que j'ai mis en scène, et je suis comédienne principale dans une compagnie en tournée jusque fin Juillet. Je recherche maintenant à produire mon nouveau spectacles pour septembre : <<les contes de Mr Poissy>>.");
  }

  //------------------------------Files-------------------------------
  public onFileChanged(event: any) {
    this.selectedFile = event.target.files[0];
  }

  onUpload() {
    console.log(this.selectedFile);
    const uploadImageData = new FormData();

    uploadImageData.append(
      'imageFile',
      this.selectedFile,
      this.selectedFile.name
    );
    this.httpClient
      .post('http://localhost:8888/image/upload', uploadImageData, {
        observe: 'response',
      })
      .subscribe((response) => {
        if (response.status === 200) {
          this.message = 'Votre image : ';
          this.form.imageName = this.selectedFile.name;
          this.imageName = this.selectedFile.name;
          this.getImage();
        } else {
          this.message = 'Image not uploaded successfully';
        }
      });
  }
  getImage() {
    this.httpClient
      .get('http://localhost:8888/image/get/' + this.imageName)

      .subscribe((res) => {
        this.retrieveResonse = res;

        this.base64Data = this.retrieveResonse.picByte;

        this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
      });
  }

  //------------------------------SelectDepartments-------------------------------
  selectMyDep(event: any) {
    this.form.location = event.target.value;
    this.selectedDepartements.push(this.form.location);
  }

  selectDep(event: any) {
    this.selectedDepartment = event.target.value;
    this.selectedDepartements.push(this.selectedDepartment);
  }

  deleteDep(dep: number) {
    this.selectedDepartements.splice(dep, 1);
  }

  //------------------------------SelectDomains-------------------------------

  selectMyDomain(event: any) {
    this.form.domain = event.target.value;
    this.selectedDomains.push(this.form.domain);
  }

  selectDomain(event: any) {
    this.selectedDomain = event.target.value;
    this.selectedDomains.push(this.selectedDomain);
  }

  deleteDomain(dom: number) {
    this.selectedDomains.splice(dom, 1);
  }
}
