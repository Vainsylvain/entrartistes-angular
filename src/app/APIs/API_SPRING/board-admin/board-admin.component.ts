import { Component, OnInit } from '@angular/core';
import { ProfilService } from 'src/app/common/services/profil.service';

@Component({
  selector: 'app-board-admin',
  templateUrl: './board-admin.component.html',
  styleUrls: ['./board-admin.component.scss']
})
export class BoardAdminComponent implements OnInit {

  content?: string;
  constructor(private profilService: ProfilService) { }
  ngOnInit(): void {
  }

}
