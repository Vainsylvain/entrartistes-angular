import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardArtistComponent } from './board-artist.component';

describe('BoardArtistComponent', () => {
  let component: BoardArtistComponent;
  let fixture: ComponentFixture<BoardArtistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardArtistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardArtistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
