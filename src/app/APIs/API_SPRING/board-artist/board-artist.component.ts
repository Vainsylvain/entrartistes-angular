import { Component, OnInit } from '@angular/core';
import { ProfilService } from 'src/app/common/services/profil.service';
import { TokenStorageService } from 'src/app/common/services/token-storage.service';

@Component({
  selector: 'app-board-artist',
  templateUrl: './board-artist.component.html',
  styleUrls: ['./board-artist.component.scss']
})
export class BoardArtistComponent implements OnInit {

  listDeps : String[] = [];
  listDoms : String[] = [];

  constructor(private profilService: ProfilService, private tokenStorage: TokenStorageService) { }

  ngOnInit(): void {

    const user = this.tokenStorage.getUser();
    const username = user.username;

    this.profilService.getArtistPrefs(username).subscribe({
      next: data => {
        console.log(data);
        this.listDeps = data.locations;
        this.listDoms = data.domains;
        console.log(this.listDeps);
        console.log(this.listDoms);
      },
      error: err => {
      }
    });

  }

}
