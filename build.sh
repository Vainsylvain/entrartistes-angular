#!/bin/sh
if [ $1 = "production" ];then
    echo "production"
    cp src/index.html public/index.html
    cp -rp src package.json angular.json public
#    sed -i -r "s/.*url.*/        url: $DESTINY_CARDS_API,/g" public/js/main.js
else
    echo "staging"
    rm -rf public/staging || true
    mkdir public/staging
    cp src/index.html public/index.html
    cp -rp src package.json angular.json public/staging
#    sed -i -r "s/.*url.*/        url: $DESTINY_CARDS_STAGING_API,/g" public/staging/js/main.js
fi
